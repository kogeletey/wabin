def clone [repo_name: string, version: string, app_name: string] {
    git clone --depth=1 --branch=$version $repo_name $app_name
}

def build [out_dir: string, app_name: string] {
            if not ($"$app_name-app" | path exists) {
            clone
    }
}

# TODO: install tooling

def pack_tauri[app_name: string, package_manager: string, targets: string = all] {
        if not ("$app_name-dist" | path exists) {
                main build
        }
        cd $"(pwd)/$app_name-dist"
        $package_manager pake-cli . --name=$app_name --iter-copy-file --targets $targets
        mv *.{appImage,deb} ..
}

