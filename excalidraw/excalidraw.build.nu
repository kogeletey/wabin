def main [name] {
    match $name {
        clone => {
            git clone https://github.com/excalidraw/excalidraw --depth=1 --branch=v0.17.0 excalidraw-app
            "excalidraw-app" | save --append .gitignore
        },
        build => {
            if not ("excalidraw-app" | path exists) {
                main clone
            }
            cd excalidraw-app
            yarn install
            yarn build
            mv build ../excalidraw-dist
            cd ..
            rm -rf excalidraw-app
            "\nexcalidraw-dist" | save --append .gitignore
        }
        pack:tauri => {
            if not ("excalidraw-dist" | path exists) {
                main build
            }
            cd $"(pwd)/excalidraw-dist"
            bunx pake-cli . --name=excalidraw --iter-copy-file --targets all
            mv *.{appImage,deb} ..
        }
    }
}
